package com.teddyguitarhero.offeraggregatorapp.service;

import com.teddyguitarhero.offeraggregatorapp.ApiConfig;
import com.teddyguitarhero.offeraggregatorapp.model.CommonApplication;
import com.teddyguitarhero.offeraggregatorapp.model.CommonApplicationRequest;
import com.teddyguitarhero.offeraggregatorapp.model.CommonOffer;
import com.teddyguitarhero.offeraggregatorapp.model.OfferResult;
import com.teddyguitarhero.offeraggregatorapp.service.api.FastBankService;
import com.teddyguitarhero.offeraggregatorapp.service.api.SolidBankService;
import com.teddyguitarhero.offeraggregatorapp.test.samples.SampleApplication;
import org.awaitility.Awaitility;
import org.awaitility.core.DurationFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.springframework.http.HttpStatus;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.client.RestClientResponseException;

import java.nio.charset.Charset;
import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;

import static com.teddyguitarhero.offeraggregatorapp.model.OfferResult.OfferResultStatus.*;
import static com.teddyguitarhero.offeraggregatorapp.test.samples.SampleApplication.approvedOffer;
import static com.teddyguitarhero.offeraggregatorapp.test.samples.SampleApplication.draftOffer;
import static com.teddyguitarhero.offeraggregatorapp.test.samples.SampleApplicationRequest.sampleApplicationRequest;
import static com.teddyguitarhero.offeraggregatorapp.test.service.AwaitUntilExecutionCompletes.awaitAtMost;
import static com.teddyguitarhero.offeraggregatorapp.test.service.OfferBuilder.offer;
import static java.time.Duration.ZERO;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@DisplayName("OfferAggregationService")
@ExtendWith(MockitoExtension.class)
class OfferAggregationServiceTest {

    private final long backoffInterval = 100L;
    private final long toleranceInterval = 2 * backoffInterval;
    private final long timeout = 2 * 1000L;
    private final Duration almostImmediately = DurationFactory.of(toleranceInterval, MILLISECONDS);
    private final Duration longestProcessingTime = DurationFactory.of(timeout + toleranceInterval, MILLISECONDS);

    private FastBankService fastBankService;
    private SolidBankService solidBankService;

    private OfferAggregationService service;

    @BeforeEach
    void setUp() {
        Awaitility.setDefaultPollDelay(ZERO);

        fastBankService = mock(FastBankService.class);
        solidBankService = mock(SolidBankService.class);
        RetryTemplate retryTemplate = ApiConfig.buildRetryTemplate(backoffInterval);
        ApiProperties apiProperties = new ApiProperties(
                DurationFactory.of(backoffInterval, MILLISECONDS),
                DurationFactory.of(timeout, MILLISECONDS)
        );

        service = new OfferAggregationService(fastBankService, solidBankService, apiProperties, retryTemplate);

        lenient().when(fastBankService.getName())
                .thenReturn("Fast Bank Mock");

        lenient().when(solidBankService.getName())
                .thenReturn("Solid Bank Mock");
    }

    @Nested
    @DisplayName("should map to error status")
    class MapToErrorStatus {

        @Test
        @DisplayName("when got expected error response on submit")
        void shouldMapToErrorStatusWhenGotUnexpectedErrorResponseOnSubmit() {
            when(fastBankService.submitApplication(any()))
                    .thenThrow(badRequestResponseException());

            when(solidBankService.submitApplication(any()))
                    .thenThrow(badRequestResponseException());

            List<OfferResult> offerResults = awaitAtMost(almostImmediately)
                    .toComplete(collectingOffersFor(sampleApplicationRequest()));

            assertThat(offerResults).containsExactly(
                    offer().from(fastBankService).withStatus(API_ERROR).and(noOffer()),
                    offer().from(solidBankService).withStatus(API_ERROR).and(noOffer())
            );
        }

        @Test
        @DisplayName("when got unexpected error response on submit")
        void shouldMapToUnexpectedErrorWhenGotUnexpectedErrorResponseOnSubmit() {
            when(fastBankService.submitApplication(any()))
                    .thenThrow(notFoundResponseException());

            when(solidBankService.submitApplication(any()))
                    .thenThrow(internalServerErrorResponseException());

            List<OfferResult> offerResults = awaitAtMost(almostImmediately)
                    .toComplete(collectingOffersFor(sampleApplicationRequest()));

            assertThat(offerResults).containsExactly(
                    offer().from(fastBankService).withStatus(UNEXPECTED_ERROR).and(noOffer()),
                    offer().from(solidBankService).withStatus(UNEXPECTED_ERROR).and(noOffer())
            );
        }

        @Test
        @DisplayName("when got error on first polling call")
        void shouldMapToErrorResponseWhenGotErrorOnPoll() {
            when(fastBankService.submitApplication(any()))
                    .thenReturn(draftOffer());

            when(fastBankService.retrieveApplication(any()))
                    .thenThrow(badRequestResponseException());

            when(solidBankService.submitApplication(any()))
                    .thenReturn(draftOffer());

            when(solidBankService.retrieveApplication(any()))
                    .thenThrow(notFoundResponseException());

            List<OfferResult> offerResults = awaitAtMost(almostImmediately)
                    .toComplete(collectingOffersFor(sampleApplicationRequest()));

            assertThat(offerResults).containsExactly(
                    offer().from(fastBankService).withStatus(API_ERROR).and(noOffer()),
                    offer().from(solidBankService).withStatus(API_ERROR).and(noOffer())
            );
        }

        @Test
        @DisplayName("when got unexpected error response on submit")
        void shouldMapToUnexpectedErrorWhenGotUnexpectedErrorResponseDuringPolling() {
            when(fastBankService.submitApplication(any()))
                    .thenReturn(draftOffer());

            when(fastBankService.retrieveApplication(any()))
                    .thenThrow(notFoundResponseException());

            when(solidBankService.submitApplication(any()))
                    .thenReturn(draftOffer());

            when(solidBankService.retrieveApplication(any()))
                    .thenThrow(internalServerErrorResponseException());

            List<OfferResult> offerResults = awaitAtMost(almostImmediately)
                    .toComplete(collectingOffersFor(sampleApplicationRequest()));

            assertThat(offerResults).containsExactly(
                    offer().from(fastBankService).withStatus(API_ERROR).and(noOffer()),
                    offer().from(solidBankService).withStatus(UNEXPECTED_ERROR).and(noOffer())
            );
        }

        @Test
        @DisplayName("when ran out of retries")
        void shouldMapToErrorResponseWhenRanOutOfRetries() {
            when(fastBankService.submitApplication(any()))
                    .thenReturn(draftOffer());

            when(fastBankService.retrieveApplication(any()))
                    .thenReturn(draftOffer());

            when(solidBankService.submitApplication(any()))
                    .thenReturn(draftOffer());

            when(solidBankService.retrieveApplication(any()))
                    .thenReturn(draftOffer());

            List<OfferResult> offerResults = awaitAtMost(longestProcessingTime)
                    .toComplete(collectingOffersFor(sampleApplicationRequest()));

            assertThat(offerResults).containsExactly(
                    offer().from(fastBankService).withStatus(TIMED_OUT).and(noOffer()),
                    offer().from(solidBankService).withStatus(TIMED_OUT).and(noOffer())
            );
        }
    }

    @Nested
    @DisplayName("should map to declined result")
    class MapToDeclinedResponse {

        @Test
        @DisplayName("when got null offer right after submission")
        void shouldMapToDeclinedResultWhenGotNullOfferRightAfterSubmission() {
            when(fastBankService.submitApplication(any()))
                    .thenReturn(SampleApplication.declinedOffer());

            when(solidBankService.submitApplication(any()))
                    .thenReturn(draftOffer());

            when(solidBankService.retrieveApplication(any()))
                    .thenReturn(draftOffer());

            List<OfferResult> offerResults = awaitAtMost(longestProcessingTime)
                    .toComplete(collectingOffersFor(sampleApplicationRequest()));

            assertThat(offerResults).containsExactly(
                    offer().from(fastBankService).withStatus(DECLINED).and(noOffer()),
                    offer().from(solidBankService).withStatus(TIMED_OUT).and(noOffer())
            );
        }

        @Test
        @DisplayName("when got null offer during polling")
        void shouldMapToDeclinedResultWhenGotNullOfferDuringPolling() {
            when(fastBankService.submitApplication(any()))
                    .thenReturn(draftOffer());

            when(fastBankService.retrieveApplication(any()))
                    .thenReturn(SampleApplication.declinedOffer());

            when(solidBankService.submitApplication(any()))
                    .thenReturn(draftOffer());

            when(solidBankService.retrieveApplication(any()))
                    .thenReturn(draftOffer());

            List<OfferResult> offerResults = awaitAtMost(longestProcessingTime)
                    .toComplete(collectingOffersFor(sampleApplicationRequest()));

            assertThat(offerResults).containsExactly(
                    offer().from(fastBankService).withStatus(DECLINED).and(noOffer()),
                    offer().from(solidBankService).withStatus(TIMED_OUT).and(noOffer())
            );
        }
    }

    @Nested
    @DisplayName("should map to offered result")
    class MapToOfferedResult {

        @Test
        @DisplayName("when got offer right after submission")
        void shouldMapToOfferedResultWhenGotOfferRightAfterSubmission() {
            when(fastBankService.submitApplication(any()))
                    .thenReturn(approvedOffer());

            when(solidBankService.submitApplication(any()))
                    .thenReturn(draftOffer());

            when(solidBankService.retrieveApplication(any()))
                    .thenReturn(draftOffer());

            List<OfferResult> offerResults = awaitAtMost(longestProcessingTime)
                    .toComplete(collectingOffersFor(sampleApplicationRequest()));

            assertThat(offerResults).containsExactly(
                    offer().from(fastBankService).withStatus(OFFERED).and(offerFrom(approvedOffer())),
                    offer().from(solidBankService).withStatus(TIMED_OUT).and(noOffer())
            );
        }

        @Test
        @DisplayName("when got offer on first polling call")
        void shouldMapToOfferedResultWhenGotOfferOnFirstPolling() {
            when(fastBankService.submitApplication(any()))
                    .thenReturn(draftOffer());

            when(fastBankService.retrieveApplication(any()))
                    .thenReturn(approvedOffer());

            when(solidBankService.submitApplication(any()))
                    .thenReturn(draftOffer());

            when(solidBankService.retrieveApplication(any()))
                    .thenReturn(draftOffer());

            List<OfferResult> offerResults = awaitAtMost(longestProcessingTime)
                    .toComplete(collectingOffersFor(sampleApplicationRequest()));

            assertThat(offerResults).containsExactly(
                    offer().from(fastBankService).withStatus(OFFERED).and(offerFrom(approvedOffer())),
                    offer().from(solidBankService).withStatus(TIMED_OUT).and(noOffer())
            );
        }
    }

    @Nested
    @DisplayName("should complete processing quickly when")
    class CompleteProcessingQuickly {

        @Test
        @DisplayName("all services responded on submit")
        void shouldQuicklyMapToOfferedResultWhenAllRespondedOnSubmit() {
            when(fastBankService.submitApplication(any()))
                    .thenReturn(SampleApplication.declinedOffer());

            when(solidBankService.submitApplication(any()))
                    .thenReturn(approvedOffer());

            List<OfferResult> offerResults = awaitAtMost(almostImmediately)
                    .toComplete(collectingOffersFor(sampleApplicationRequest()));

            assertThat(offerResults).containsExactly(
                    offer().from(fastBankService).withStatus(DECLINED).and(noOffer()),
                    offer().from(solidBankService).withStatus(OFFERED).and(offerFrom(approvedOffer()))
            );
        }

        @Test
        @DisplayName("all services responded on first polling call")
        void shouldQuicklyMapToOfferedResultWhenAllRespondedOnFirstPolling() {
            when(fastBankService.submitApplication(any()))
                    .thenReturn(draftOffer());

            when(fastBankService.retrieveApplication(any()))
                    .thenReturn(SampleApplication.declinedOffer());

            when(solidBankService.submitApplication(any()))
                    .thenReturn(draftOffer());

            when(solidBankService.submitApplication(any()))
                    .thenReturn(approvedOffer());

            List<OfferResult> offerResults = awaitAtMost(almostImmediately)
                    .toComplete(collectingOffersFor(sampleApplicationRequest()));

            assertThat(offerResults).containsExactly(
                    offer().from(fastBankService).withStatus(DECLINED).and(noOffer()),
                    offer().from(solidBankService).withStatus(OFFERED).and(offerFrom(approvedOffer()))
            );
        }

        @Test
        @DisplayName("longest service polling completed before timeout")
        void shouldQuicklyMapToOfferedResultWhenLongestRespondedDuringPolling() {
            when(fastBankService.submitApplication(any()))
                    .thenReturn(draftOffer());

            when(fastBankService.retrieveApplication(any()))
                    .thenReturn(SampleApplication.declinedOffer());

            when(solidBankService.submitApplication(any()))
                    .thenReturn(draftOffer());

            when(solidBankService.retrieveApplication(any()))
                    .thenAnswer(new Answer<CommonApplication>() {

                        private int count = 0;

                        @Override
                        public CommonApplication answer(InvocationOnMock invocation) {
                            count++;

                            if (count <= 5) {
                                return draftOffer();
                            } else {
                                return approvedOffer();
                            }
                        }
                    });

            Duration timeNeededToCompleteFiveRetries = DurationFactory.of(
                    5 * backoffInterval + toleranceInterval,
                    MILLISECONDS
            );

            List<OfferResult> offerResults = awaitAtMost(timeNeededToCompleteFiveRetries)
                    .toComplete(collectingOffersFor(sampleApplicationRequest()));

            assertThat(offerResults).containsExactly(
                    offer().from(fastBankService).withStatus(DECLINED).and(noOffer()),
                    offer().from(solidBankService).withStatus(OFFERED).and(offerFrom(approvedOffer()))
            );
        }

    }

    private RestClientResponseException badRequestResponseException() {
        return restClientResponseException(HttpStatus.BAD_REQUEST);
    }

    private RestClientResponseException notFoundResponseException() {
        return restClientResponseException(HttpStatus.NOT_FOUND);
    }

    private RestClientResponseException internalServerErrorResponseException() {
        return restClientResponseException(INTERNAL_SERVER_ERROR);
    }

    private RestClientResponseException restClientResponseException(HttpStatus httpStatus) {
        return new RestClientResponseException(
                "Application was slightly invalid",
                httpStatus.value(),
                httpStatus.getReasonPhrase(),
                null,
                null,
                Charset.defaultCharset()
        );
    }

    private Callable<List<OfferResult>> collectingOffersFor(CommonApplicationRequest applicationRequest) {
        return () -> service.applyForOffers(applicationRequest);
    }

    private Optional<CommonOffer> noOffer() {
        return Optional.empty();
    }

    private Optional<CommonOffer> offerFrom(CommonApplication application) {
        return Optional.of(application.getOffer());
    }
}
