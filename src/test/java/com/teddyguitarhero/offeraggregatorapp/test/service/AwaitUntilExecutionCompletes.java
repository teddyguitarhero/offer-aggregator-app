package com.teddyguitarhero.offeraggregatorapp.test.service;

import org.awaitility.core.DurationFactory;

import java.time.Duration;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import static com.teddyguitarhero.offeraggregatorapp.test.service.Functions.executionCompletes;
import static org.awaitility.Awaitility.await;

public class AwaitUntilExecutionCompletes {

    public static ConditionFactory awaitAtMost(long timeout, TimeUnit unit) {
        return new ConditionFactory(DurationFactory.of(timeout, unit));
    }

    public static ConditionFactory awaitAtMost(Duration timeout) {
        return new ConditionFactory(timeout);
    }

    @SuppressWarnings("ClassCanBeRecord")
    public static class ConditionFactory {

        private final Duration timeout;

        private ConditionFactory(Duration timeout) {
            this.timeout = timeout;
        }

        public <T> T toComplete(Callable<T> supplier) {
            return await()
                    .atMost(timeout)
                    .until(supplier, executionCompletes());
        }
    }
}
