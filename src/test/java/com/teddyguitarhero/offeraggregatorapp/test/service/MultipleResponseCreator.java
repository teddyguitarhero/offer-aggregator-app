package com.teddyguitarhero.offeraggregatorapp.test.service;

import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.test.web.client.ResponseCreator;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

public class MultipleResponseCreator implements ResponseCreator {

    private final Deque<ResponseCreator> creators;

    public static MultipleResponseCreator withResponses(ResponseCreator... creators) {
        return new MultipleResponseCreator(creators);
    }

    public MultipleResponseCreator(ResponseCreator... creators) {
        this.creators = new ArrayDeque<>(Arrays.asList(creators));
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public ClientHttpResponse createResponse(ClientHttpRequest request) throws IOException {
        if (creators.isEmpty()) {
            throw new IllegalStateException("No response creators left in the queue");
        }

        return creators.pop().createResponse(request);
    }
}
