package com.teddyguitarhero.offeraggregatorapp.test.service;

import com.teddyguitarhero.offeraggregatorapp.model.CommonOffer;
import com.teddyguitarhero.offeraggregatorapp.model.OfferResult;
import com.teddyguitarhero.offeraggregatorapp.service.OfferProvider;

import java.util.Optional;

public class OfferBuilder {

    private String providerName;
    private OfferResult.OfferResultStatus status;

    public static OfferBuilder offer() {
        return new OfferBuilder();
    }

    public OfferBuilder from(OfferProvider provider) {
        this.providerName = provider.getName();
        return this;
    }

    public OfferBuilder withStatus(OfferResult.OfferResultStatus status) {
        this.status = status;
        return this;
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public OfferResult and(Optional<CommonOffer> offer) {
        return new OfferResult(this.providerName, this.status, offer);
    }
}
