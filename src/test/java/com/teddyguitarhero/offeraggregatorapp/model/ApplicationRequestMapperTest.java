package com.teddyguitarhero.offeraggregatorapp.model;

import org.junit.jupiter.api.Test;

import static com.teddyguitarhero.offeraggregatorapp.test.samples.SampleApplicationRequest.sampleApplicationRequest;
import static org.assertj.core.api.Assertions.assertThat;

class ApplicationRequestMapperTest {

    private final ApplicationRequestMapper mapper = new ApplicationRequestMapperImpl();

    @Test
    void toFastBankApplicationRequest() {
        CommonApplicationRequest original = sampleApplicationRequest();

        org.fastbank.client.model.ApplicationRequest actual
                = mapper.toFastBankApplicationRequest(original);

        assertThat(actual.getPhoneNumber()).isEqualTo(original.getPhone());
        assertThat(actual.getEmail()).isEqualTo(original.getEmail());
        assertThat(actual.getMonthlyIncomeAmount()).isEqualTo(original.getMonthlyIncome());
        assertThat(actual.getMonthlyCreditLiabilities()).isEqualTo(original.getMonthlyCreditLiabilities());
        assertThat(actual.getDependents()).isEqualTo(original.getDependents());
        assertThat(actual.getAgreeToDataSharing()).isEqualTo(original.getAgreeToDataSharing());
        assertThat(actual.getAmount()).isEqualTo(original.getAmount());
    }

    @Test
    void toSolidBankApplicationRequest() {
        CommonApplicationRequest original = sampleApplicationRequest();

        org.solidbank.client.model.ApplicationRequest actual
                = mapper.toSolidBankApplicationRequest(original);

        assertThat(actual.getPhone()).isEqualTo(original.getPhone());
        assertThat(actual.getEmail()).isEqualTo(original.getEmail());
        assertThat(actual.getMonthlyIncome()).isEqualTo(original.getMonthlyIncome());
        assertThat(actual.getMonthlyExpenses()).isEqualTo(original.getMonthlyExpenses());
        assertThat(actual.getMaritalStatus()).isEqualTo(original.getMaritalStatus());
        assertThat(actual.getAgreeToBeScored()).isEqualTo(original.getAgreeToBeScored());
        assertThat(actual.getAmount()).isEqualTo(original.getAmount());
    }
}
