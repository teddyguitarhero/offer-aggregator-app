package com.teddyguitarhero.offeraggregatorapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfferAggregatorApp {

    public static void main(String[] args) {
        SpringApplication.run(OfferAggregatorApp.class, args);
    }

}
