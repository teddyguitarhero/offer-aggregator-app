package com.teddyguitarhero.offeraggregatorapp.service.api;

import org.solidbank.ApiClient;
import org.solidbank.api.DefaultApi;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SolidBankApi extends DefaultApi {

    public SolidBankApi(RestTemplate bankApiRestTemplate) {
        super(new ApiClient(bankApiRestTemplate));
    }
}
