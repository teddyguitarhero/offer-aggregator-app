package com.teddyguitarhero.offeraggregatorapp.service;

import com.teddyguitarhero.offeraggregatorapp.model.OfferResult;
import lombok.EqualsAndHashCode;
import lombok.Value;


@Value
@EqualsAndHashCode(callSuper = true)
public class OfferError extends RuntimeException {

    String providerName;
    OfferResult.OfferResultStatus status;

    public OfferError(String providerName, OfferResult.OfferResultStatus status) {
        this.providerName = providerName;
        this.status = status;
    }

    public OfferError(String providerName, OfferResult.OfferResultStatus status, Throwable cause) {
        super(cause);
        this.providerName = providerName;
        this.status = status;
    }
}
