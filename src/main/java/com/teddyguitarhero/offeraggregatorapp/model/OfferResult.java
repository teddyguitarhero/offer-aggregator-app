package com.teddyguitarhero.offeraggregatorapp.model;

import lombok.Value;

import java.util.Optional;

@SuppressWarnings("ClassCanBeRecord")
@Value
public class OfferResult {

    String providerName;
    OfferResultStatus status;
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    Optional<CommonOffer> offer;

    public enum OfferResultStatus {
        OFFERED,
        DECLINED,
        TIMED_OUT,
        API_ERROR,
        UNEXPECTED_ERROR
    }
}
