package com.teddyguitarhero.offeraggregatorapp.model;

import lombok.Value;

import java.math.BigDecimal;

@SuppressWarnings("ClassCanBeRecord")
@Value
public class CommonOffer {

    BigDecimal monthlyPaymentAmount;
    BigDecimal totalRepaymentAmount;
    Integer numberOfPayments;
    BigDecimal annualPercentageRate;
    String firstRepaymentDate;
}
