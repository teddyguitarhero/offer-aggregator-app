package com.teddyguitarhero.offeraggregatorapp.model;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ApplicationRequestMapper {

    @Mapping(source = "phone", target = "phoneNumber")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "monthlyIncome", target = "monthlyIncomeAmount")
    @Mapping(source = "monthlyCreditLiabilities", target = "monthlyCreditLiabilities")
    @Mapping(source = "dependents", target = "dependents")
    @Mapping(source = "agreeToDataSharing", target = "agreeToDataSharing")
    @Mapping(source = "amount", target = "amount")
    org.fastbank.client.model.ApplicationRequest toFastBankApplicationRequest(CommonApplicationRequest request);

    @Mapping(source = "phone", target = "phone")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "monthlyIncome", target = "monthlyIncome")
    @Mapping(source = "monthlyExpenses", target = "monthlyExpenses")
    @Mapping(source = "maritalStatus", target = "maritalStatus")
    @Mapping(source = "agreeToBeScored", target = "agreeToBeScored")
    @Mapping(source = "amount", target = "amount")
    org.solidbank.client.model.ApplicationRequest toSolidBankApplicationRequest(CommonApplicationRequest request);
}
