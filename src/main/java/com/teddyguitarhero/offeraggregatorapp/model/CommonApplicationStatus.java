package com.teddyguitarhero.offeraggregatorapp.model;

public enum CommonApplicationStatus {
    DRAFT,
    PROCESSED
}
