package com.teddyguitarhero.offeraggregatorapp.model;

import lombok.Value;

import java.util.UUID;

import static com.teddyguitarhero.offeraggregatorapp.model.CommonApplicationStatus.PROCESSED;

@SuppressWarnings("ClassCanBeRecord")
@Value
public class CommonApplication {

    UUID id;
    CommonApplicationStatus status;
    CommonOffer offer;

    public boolean isProcessed() {
        return status == PROCESSED;
    }
}
