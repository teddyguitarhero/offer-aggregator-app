package com.teddyguitarhero.offeraggregatorapp;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.teddyguitarhero.offeraggregatorapp.service.ApiProperties;
import com.teddyguitarhero.offeraggregatorapp.service.ApplicationStillProcessingException;
import lombok.RequiredArgsConstructor;
import org.openapitools.jackson.nullable.JsonNullableModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Configuration
@RequiredArgsConstructor
public class ApiConfig {

    private final ApiProperties apiProperties;

    @Bean
    public RestTemplate bankApiRestTemplate() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JsonNullableModule());
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter(objectMapper);

        RestTemplate restTemplate = new RestTemplate();
        // This allows us to read the response more than once - Necessary for debugging.
        restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(restTemplate.getRequestFactory()));

        restTemplate.setMessageConverters(List.of(messageConverter));

        return restTemplate;
    }

    @Bean
    public RetryTemplate bankApiRetryTemplate() {
        return buildRetryTemplate(apiProperties.getBackoffInterval().toMillis());
    }

    public static RetryTemplate buildRetryTemplate(long backoffInterval) {
        return RetryTemplate.builder()
                .retryOn(ApplicationStillProcessingException.class)
                .infiniteRetry()
                .fixedBackoff(backoffInterval)
                .build();
    }
}

